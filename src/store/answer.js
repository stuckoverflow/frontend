import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import http from '@/service/http'
import auth from '@/service/auth'
import { toast } from 'react-toastify'

var initialState = {
	name: 'answer',
	data: [],
	show: {}
}

export const answerGet = createAsyncThunk('answer/get', async(quest) => {
	var data = await http(import.meta.env.VITE_API_URL + 'answer?quest=' + quest)
	return await data
})
export const answerGetUser = createAsyncThunk('answer/get/user', async(page) => {
	var data = await http(import.meta.env.VITE_API_URL + 'answer/user?page=' + page)
	return await data
})
export const answerGetId = createAsyncThunk('answer/get/id', async(id) => {
	var data = await http(import.meta.env.VITE_API_URL + 'answer/' + id + '/id')
	return await data
})
export const answerAdd = createAsyncThunk('answer/add', async(body, { rejectWithValue, fulfillWithValue }) => {
	var data
	try{
		data = await http(import.meta.env.VITE_API_URL + 'answer', {
			method: 'POST',
			data: {...body, user_id: auth().user._id},
		})
		toast.success(data.message, {
			position: toast.POSITION.BOTTOM_RIGHT
		})
		return fulfillWithValue(data)

	}catch(err){
		err.response.data.error.map((obj) => {
			toast.error(obj.msg, {
				position: toast.POSITION.BOTTOM_RIGHT
			})
		})
		return rejectWithValue(err.response.data)
	}
})
export const answerUpdate = createAsyncThunk('answer/update', async(body) => {
	var data = await http(import.meta.env.VITE_API_URL + 'answer/' + body.id, {
		method: 'PATCH',
		data: {...body, user_id: auth().user._id},
	})
	return await data
})
export const answerDelete = createAsyncThunk('answer/delete', async(body, thunkAPI) => {
	var data
	try{
		data = await http(import.meta.env.VITE_API_URL + 'answer/' + body._id, {
			method: 'DELETE',
		})
		toast.success(data.message, {
			position: toast.POSITION.BOTTOM_RIGHT
		})
		return fulfillWithValue(data)
	}catch(err){
		err.response.data.error.map((obj) => {
			toast.error(obj.msg, {
				position: toast.POSITION.BOTTOM_RIGHT
			})
		})
		return rejectWithValue(err.response.data)
	}
})

export const answerSlice = createSlice({
	name: 'answer',
	initialState,
	reducers: {
		handle(state, action){
			state[action.payload.name] = action.payload.value
		}
	},
	extraReducers: {
    	[answerGet.fulfilled]: (state, action) => {
    		state.data = action.payload.data
    	},
    	[answerGetUser.fulfilled]: (state, action) => {
    		state.data = action.payload.data
    	},
    	[answerGetId.fulfilled]: (state, action) => {
    		state.show = action.payload.data
    	},
    	[answerAdd.fulfilled]: (state, action) => {
    		state.data.push(action.payload)
    	},
    	[answerUpdate.fulfilled]: (state, action) => {
    		state.data = state.data.map(e => {
    			if(e._id === action.payload.input._id){
    				e = action.payload.data.input
    			}
    			return e
    		})
    	},
    	[answerDelete.fulfilled]: (state, action) => {
    		state.data = state.data.filter(e => e._id !== action.payload._id)
    	},
  	},
})
export const {handle} = answerSlice.actions

export default answerSlice.reducer