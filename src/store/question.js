import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import http from '@/service/http'
import auth from '@/service/auth'
import { toast } from 'react-toastify'

var initialState = {
	name: 'question',
	data: [],
	show: {
		quest: '',
		views: 0
	},
	error: null
}

export const questionGet = createAsyncThunk('question/get', async(page) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question?page=' + page)
	return await data
})
export const questionShow = createAsyncThunk('question/show', async(id) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question/' + id)
	return data
})
export const questionAdd = createAsyncThunk('question/add', async(body, {fulfillWithValue, rejectWithValue}) => {
	var data
	try{
		data = await http(import.meta.env.VITE_API_URL + 'question', {
			method: 'POST',
			data: {...body, user_id: auth().user._id},
		})
		toast.success(data.message, {
			position: toast.POSITION.BOTTOM_RIGHT
		})
		return fulfillWithValue(data)

	}catch(err){
		err.response.data.error.map((obj) => {
			toast.error(obj.msg, {
				position: toast.POSITION.BOTTOM_RIGHT
			})
		})
		return rejectWithValue(err.response.data)
	}
})
export const questionUpdate = createAsyncThunk('question/update', async(body) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question/' + body.id, {
		method: 'PATCH',
		data: {...body, user_id: auth().user._id},
	})
	return body
})
export const questionDelete = createAsyncThunk('question/delete', async(body) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question/' + body.id, {
		method: 'DELETE',
	})
	return body
})

export const questionSlice = createSlice({
	name: 'question',
	initialState,
	reducers: {
		handle(state, action){
			state[action.payload.name] = action.payload.value
		}
	},
	extraReducers: {
    	[questionGet.fulfilled]: (state, action) => {
    		state.data = action.payload.data
    	},
    	[questionShow.fulfilled]: (state, action) => {
    		state.show = action.payload.data
    	},
    	[questionAdd.fulfilled]: (state, action) => {
    		state.data.push(action.payload.input)
    	},
    	[questionAdd.rejected]: (state, action) => {
    		state.error = action.error.message
    	},
    	[questionUpdate.fulfilled]: (state, action) => {
    		state.data = state.data.map(e => {
    			if(e._id === action.payload._id){
    				e = Object.assign(e, action.payload)
    			}
    			return e
    		})
    	},
    	[questionDelete.fulfilled]: (state, action) => {
    		state.data = state.data.filter(e => e.id !== action.payload)
    	},
  	},
})
export const {handle} = questionSlice.actions

export default questionSlice.reducer