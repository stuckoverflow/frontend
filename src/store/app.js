import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

var initialState = {
	name: 'STUCKOVERFLOW',
	blog: [],
	bloglatest: []
}

export const getBlog = createAsyncThunk('blog/user', async() => {
	var data = await fetch(`https://ferdiansyah-blog.herokuapp.com/api/blog/user/17`)
	return await data.json()
})
export const getBlogLatest = createAsyncThunk('blog/latest', async() => {
	var data = await fetch(`https://ferdiansyah-blog.herokuapp.com/api/blog`)
	return await data.json()
})

export const appSlice = createSlice({
	name: 'app',
	initialState,
	reducers: {
		handle(state, action){
			state[action.payload.name] = action.payload.value
		}
	},
	extraReducers: {
		[getBlog.fulfilled]: (state, action) => {
			state.blog = action.payload
		},
		[getBlogLatest.fulfilled]: (state, action) => {
			state.bloglatest = action.payload.data
		},
	},
})
export const {} = appSlice.actions

export default appSlice.reducer