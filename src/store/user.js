import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import http from '@/service/http'
import auth from '@/service/auth'

var initialState = {
	name: 'ferdiansyah0611',
	photoURL: 'https://i.stack.imgur.com/psDIc.jpg?s=50&g=1',
	token: '',
	user: {}
}
export const signIn = createAsyncThunk('user/signin', async(data) => {
	var data = await http(import.meta.env.VITE_API_URL + 'auth/signin', {
		method: 'POST',
		data: data
	})
	return await data
})
export const signUp = createAsyncThunk('user/signup', async(data) => {
	var data = await http(import.meta.env.VITE_API_URL + 'auth/signup', {
		method: 'POST',
		data: data
	})
	return await data
})
export const logoutRequest = createAsyncThunk('user/logout', async(data) => {
	var data = await http(import.meta.env.VITE_API_URL + 'auth/logout', {
		method: 'POST'
	})
	return await data
})

export const userSlice = createSlice({
	name: 'store/user.js',
	initialState,
	reducers: {
		handle(state, action){
			state[action.payload.name] = action.payload.value
		},
		validate(state){
			var user = auth()
			if(user && !(user.expiredAt < new Date().getTime())){
				if(user?.token){
					state.user = user.user
					state.token = user.token
					state.name = user.user.name
				}
			}else{
				localStorage.removeItem('auth')
			}
		}
	},
	extraReducers: {
		[signIn.fulfilled]: (state, action) => {
			localStorage.setItem('auth', JSON.stringify(action.payload))
			state.user = action.payload.user
			state.token = action.payload.token
			state.name = action.payload.user.name
		},
		[logoutRequest.fulfilled]: (state) => {
			localStorage.removeItem('auth')
			state.user = {}
			state.token = null
			state.name = ''
		},
		[signUp.fulfilled]: (state, action) => {}
	},
})
export const {handle, validate} = userSlice.actions

export default userSlice.reducer