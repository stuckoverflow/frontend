import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import http from '@/service/http'
import auth from '@/service/auth'
import { toast } from 'react-toastify'

var initialState = {
	name: 'question-user',
	data: []
}

export const questionGet = createAsyncThunk('question-user/get', async(page) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question/user/' + auth().user._id + '?page=' + page)
	return await data
})
export const questionAdd = createAsyncThunk('question-user/add', async(body, thunkAPI) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question', {
		method: 'POST',
		data: {...body, user_id: auth().user._id},
	})
	return await data
})
export const questionUpdate = createAsyncThunk('question-user/update', async(body, thunkAPI) => {
	var data = await http(import.meta.env.VITE_API_URL + 'question/' + body.id, {
		method: 'PATCH',
		data: {...body, user_id: auth().user._id},
	})
	return body
})
export const questionDelete = createAsyncThunk('question-user/delete', async(body, {fulfillWithValue, rejectWithValue}) => {
	var data
	try{
		data = await http(import.meta.env.VITE_API_URL + 'question/' + body.id, {
			method: 'DELETE',
		})
		toast.success(data.message, {
			position: toast.POSITION.BOTTOM_RIGHT
		})
		return fulfillWithValue(body.id)

	}catch(err){
		err.response.data.error.map((obj) => {
			toast.error(obj.msg, {
				position: toast.POSITION.BOTTOM_RIGHT
			})
		})
		return rejectWithValue(err.response.data)
	}
})

export const questionSlice = createSlice({
	name: 'question-user',
	initialState,
	reducers: {
		handle(state, action){
			state[action.payload.name] = action.payload.value
		}
	},
	extraReducers: {
    	[questionGet.fulfilled]: (state, action) => {
    		state.data = action.payload.data
    	},
    	[questionAdd.fulfilled]: (state, action) => {
    		state.data.push(action.payload.input)
    	},
    	[questionUpdate.fulfilled]: (state, action) => {
    		state.data = state.data.map(e => {
    			if(e._id === action.payload._id){
    				e = Object.assign(e, action.payload)
    			}
    			return e
    		})
    	},
    	[questionDelete.fulfilled]: (state, action) => {
    		console.log(action)
    		state.data = state.data.filter(e => e._id !== action.payload)
    	},
  	},
})
export const {handle} = questionSlice.actions

export default questionSlice.reducer