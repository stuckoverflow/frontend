import React from 'react'
import {
	useDispatch, useSelector
} from 'react-redux'
import {
	Link
} from 'react-router-dom'

export default function Sideright() {
	const dispatch = useDispatch()
	const data = useSelector((state) => state.app.blog)
	const latest = useSelector((state) => state.app.bloglatest)
	const blog = React.useMemo(() => Array.from(data).slice(0,2), [data])
	const blogfeatured = React.useMemo(() => Array.from(latest).slice(0,2), [latest])
	const bloglatest = React.useMemo(() => Array.from(latest).slice(3,5), [latest])
	return(
		<div>
			<div className="section">
				<div className="item">
					<p>The Overflow Blog</p>
					{blog.map((data) => (
						<div key={data.id}>
							<i className="mdi mdi-pencil"></i>
							<a target="_blank" href={"https://ferdiansyah-blog.herokuapp.com/blog/" + data.id}>{data.title}</a>
						</div>
					))}
				</div>
				<div className="item">
					<p>Featured</p>
					{blogfeatured.map((data) => (
						<div key={data.id}>
							<i className="mdi mdi-message-outline"></i>
							<a target="_blank" href={"https://ferdiansyah-blog.herokuapp.com/blog/" + data.id}>{data.title}</a>
						</div>
					))}
				</div>
				<div className="item">
					<p>Latest Blog</p>
					{bloglatest.map(data => (
						<div key={data.id}>
							<span>{data.id}</span>
							<a target="_blank" href={"https://ferdiansyah-blog.herokuapp.com/blog/" + data.id}>{data.title}</a>
						</div>
					))}
				</div>
			</div>
		</div>
	)
}