import React from 'react'
import {
	Outlet
} from 'react-router-dom'
import {
	useSelector
} from 'react-redux'
import Navbar from './Navbar'
import Footer from './Footer'
import Sideleft from './Sideleft'
import Sideright from './Sideright'
import '@/style/home.sass'

export default function Template() {
	return(
		<div>
			<Navbar/>
			<div className="containers" id="body">
				<div className="home">
					<div className="one">
						<Sideleft/>
					</div>
					<div className="two"><Outlet/></div>
					<div className="three">
						<Sideright/>
					</div>
				</div>
			</div>
			<Footer/>
		</div>
	)
}